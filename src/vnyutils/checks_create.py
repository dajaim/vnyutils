import vnyutils.optional

import vnyutils.result


class Checks_Create:
    pass

    def __init__(self, checks, create):
        self.checks = checks
        self.create = create

        self.checks_results = []


        self.optional_result = vnyutils.optional.Empty()


    def execute(self):

        for check in self.checks:
            check_result = check(self)

            self.checks_results.append( check_result)

            match check_result:
                case vnyutils.result.Error_Result(error):
                    self.optional_result = vnyutils.optional.Present(check_result)
                    break
                case _:
                    pass

        

        match self.optional_result:
            case vnyutils.optional.Present(value):
                self.result = value
            case _:
                self.result = self.create(self)
import dataclasses


@dataclasses.dataclass
class Present:
    value: object

    def __init__(self, value):
        self.value = value


@dataclasses.dataclass
class Empty:
    pass
import dataclasses

@dataclasses.dataclass
class Value_Result:
    value: object

    def __init__(self, value):
        self.value = value


@dataclasses.dataclass
class Void_Result:
    pass


@dataclasses.dataclass
class Error_Result:
    error: object

    def __init__(self, error):
        self.error = error
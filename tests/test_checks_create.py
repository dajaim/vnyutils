import unittest

import vnyutils.result

import vnyutils.checks_create


class Test_Checks_Create(unittest.TestCase):
    def test_checks_create_with_empty_checks(self):
        Checks_Create_Class = vnyutils.checks_create.Checks_Create


        checks = []
        create = lambda checks_create_object: vnyutils.result.Void_Result()

        checks_create = Checks_Create_Class(checks, create)

        checks_create.execute()

        match checks_create.result:
            case vnyutils.result.Void_Result():
                pass
            case _:
                self.fail()



    def test_checks_create_with_void_result_returning_check(self):
        Checks_Create_Class = vnyutils.checks_create.Checks_Create

        checks = [
            lambda checks_create_object: vnyutils.result.Void_Result()
        ]

        create = lambda checks_create_object: vnyutils.result.Value_Result(5)

        checks_create = Checks_Create_Class(checks, create)

        checks_create.execute()

        match checks_create.result:
            case vnyutils.result.Value_Result(value):
                self.assertEqual(5, value)
            case _:
                self.fail()



    def test_checks_create_with_error_returning_check(self):        
        Checks_Create_Class = vnyutils.checks_create.Checks_Create

        checks = [
            lambda checks_create_object: vnyutils.result.Error_Result("error")
        ]

        create = lambda checks_create_object: vnyutils.result.Value_Result(5)

        checks_create = Checks_Create_Class(checks, create)

        checks_create.execute()


        match checks_create.result:
            case vnyutils.result.Error_Result(error):
                pass
            case _:
                self.fail()


    def test_checks_create_with_value_returning_check(self):
        Checks_Create_Class = vnyutils.checks_create.Checks_Create

        checks = [
            lambda checks_create_object: vnyutils.result.Value_Result(1)
        ]

        create = lambda checks_create_object: vnyutils.result.Value_Result(2)

        checks_create = Checks_Create_Class(checks, create)

        checks_create.execute()


        match checks_create.result:
            case vnyutils.result.Value_Result(value):
                self.assertEqual(2, value)
            case _:
                self.fail()


    def test_checks_create_with_error_returning_create(self):

        Checks_Create_Class = vnyutils.checks_create.Checks_Create

        checks = [
            lambda checks_create_object: vnyutils.result.Value_Result(1)
        ]

        create = lambda checks_create_object: vnyutils.result.Error_Result("error")

        checks_create = Checks_Create_Class(checks, create)

        checks_create.execute()


        match checks_create.result:
            case vnyutils.result.Error_Result(error):
                self.assertEqual(error, "error")
            case _:
                self.fail()



    def test_iterate_over_checks_results(self):
        
        Checks_Create_Class = vnyutils.checks_create.Checks_Create


        checks = [
            lambda _: vnyutils.result.Value_Result(1)
        ]

        create = lambda _: vnyutils.result.Value_Result(3)


        checks_create = Checks_Create_Class(checks, create)
        checks_create.execute()


        _ = list(map(lambda _: _, checks_create.checks_results))
import unittest

import vnyutils.optional

class Test_Optional(unittest.TestCase):

    def test_create_present_optional(self):
        Present_Class = vnyutils.optional.Present

        value = 5
        present_value = Present_Class(value)


        match present_value:
            case Present_Class(value):
                pass
            case _:
                self.fail()


    def test_create_empty_optional(self):
        Empty_Class = vnyutils.optional.Empty

        empty_value = Empty_Class()


        match empty_value:
            case Empty_Class():
                pass
            case _:
                self.fail()
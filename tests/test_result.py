import unittest

import vnyutils.result

class Test_Result(unittest.TestCase):
    def test_create_value_result(self):
        Value_Result_Class = vnyutils.result.Value_Result

        value = 4

        value_result = Value_Result_Class(value)


        match value_result:
            case Value_Result_Class(value):
                pass
            case _:
                self.fail()



    def test_create_void_result(self):
        Void_Result_Class = vnyutils.result.Void_Result


        void_result = Void_Result_Class()

        match void_result:
            case Void_Result_Class():
                pass
            case _:
                self.fail()



    def test_create_error_result(self):
        Error_Result_Class = vnyutils.result.Error_Result


        error_result = Error_Result_Class("error")

        match error_result:
            case Error_Result_Class(error):
                pass
            case _:
                self.fail()